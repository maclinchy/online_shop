```sh
$ yarn add graphql
$ yarn add react-relay
$ yarn add relay-compiler --dev
$ yarn add babel-plugin-relay --dev
$ yarn eject
```

`package.json`
```json
"babel": {
  "presets": [
    "react-app"
  ],
  "plugins": [
    "relay"
  ]
}
```

```sh
$ touch Environment.js
```

`Environment.js`
```js
const {
  Environment,
  Network,
  RecordSource,
  Store,
} = require('relay-runtime')

const store = new Store(new RecordSource())

const network = Network.create((operation, variables) => {
  return fetch('http://192.168.1.100:21080/graphql/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      query: operation.text,
      variables,
    }),
  }).then(response => {
    return response.json()
  })
})

const environment = new Environment({
  network,
  store,
})

export default environment
```

```sh
$ get-graphql-schema http://192.168.1.100:21080/graphql/ > ./schema.graphql
```

`package.json`
```json
"scripts": {
  "relay": "relay-compiler --src ./src --schema ./schema.graphql"
}
```

```sh
$ yarn run relay
```
