import {
  commitMutation,
  graphql,
} from 'react-relay'
import environment from '../Environment'

const mutation = graphql`
    mutation CreateItemMutation($input: ItemCreateInput! ){
      createItem(input: $input) {
        item {
          title
          description
        }
      }
    }
`

export default (title, description, callback) => {
  const variables = {
    input: {
      title,
      description,
    },
  }

  commitMutation(
    environment,
    {
      mutation,
      variables,
      onCompleted: () => {
        callback()
      },
      onError: err => console.error(err),
    },
  )
}
