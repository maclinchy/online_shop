/**
 * @flow
 * @relayHash 003acf2ed64fe71a0b1a1ec2779c97c1
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type ItemDeleteInput = {
  id: string,
  clientMutationId?: ?string,
};
export type DeleteItemMutationVariables = {|
  input: ItemDeleteInput
|};
export type DeleteItemMutationResponse = {|
  +deleteItem: ?{|
    +deletedItemId: ?string
  |}
|};
export type DeleteItemMutation = {|
  variables: DeleteItemMutationVariables,
  response: DeleteItemMutationResponse,
|};
*/


/*
mutation DeleteItemMutation(
  $input: ItemDeleteInput!
) {
  deleteItem(input: $input) {
    deletedItemId
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "ItemDeleteInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "deleteItem",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "ItemDeleteInput!"
      }
    ],
    "concreteType": "ItemDeletePayload",
    "plural": false,
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "deletedItemId",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "DeleteItemMutation",
  "id": null,
  "text": "mutation DeleteItemMutation(\n  $input: ItemDeleteInput!\n) {\n  deleteItem(input: $input) {\n    deletedItemId\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "DeleteItemMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "DeleteItemMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '064d5fdba20312f95f827dea3522432b';
module.exports = node;
