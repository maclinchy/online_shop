/**
 * @flow
 * @relayHash 9e0d5da25b12337f2b33dbf3970fba11
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type ItemCreateInput = {
  title?: ?string,
  description?: ?string,
  clientMutationId?: ?string,
};
export type CreateItemMutationVariables = {|
  input: ItemCreateInput
|};
export type CreateItemMutationResponse = {|
  +createItem: ?{|
    +item: ?{|
      +title: string,
      +description: string,
    |}
  |}
|};
export type CreateItemMutation = {|
  variables: CreateItemMutationVariables,
  response: CreateItemMutationResponse,
|};
*/


/*
mutation CreateItemMutation(
  $input: ItemCreateInput!
) {
  createItem(input: $input) {
    item {
      title
      description
      id
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "ItemCreateInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input",
    "type": "ItemCreateInput!"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "title",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "description",
  "args": null,
  "storageKey": null
};
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "CreateItemMutation",
  "id": null,
  "text": "mutation CreateItemMutation(\n  $input: ItemCreateInput!\n) {\n  createItem(input: $input) {\n    item {\n      title\n      description\n      id\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "CreateItemMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createItem",
        "storageKey": null,
        "args": v1,
        "concreteType": "ItemCreatePayload",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "item",
            "storageKey": null,
            "args": null,
            "concreteType": "ItemNode",
            "plural": false,
            "selections": [
              v2,
              v3
            ]
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "CreateItemMutation",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "createItem",
        "storageKey": null,
        "args": v1,
        "concreteType": "ItemCreatePayload",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "item",
            "storageKey": null,
            "args": null,
            "concreteType": "ItemNode",
            "plural": false,
            "selections": [
              v2,
              v3,
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "id",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '7a169c6562d999df07eaffc706ec5986';
module.exports = node;
