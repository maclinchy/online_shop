import {
  commitMutation,
  graphql
} from 'react-relay'
import environment from '../Environment'

const mutation = graphql`
    mutation DeleteItemMutation($input: ItemDeleteInput! ){
      deleteItem(input: $input) {
        deletedItemId
      }
    }
`

export default (id, callback) => {
  const variables = {
    input: {
      id,
    },
  }

  commitMutation(
    environment,
    {
      mutation,
      variables,
      onCompleted: () => {
        callback()
      },
      onError: err => console.error(err),
    },
  )

}
