import React, { Component } from 'react'

/* Mutations */
import DeleteItemMutation from '../Mutations/DeleteItemMutation'

class Item extends Component {
  render() {
    const {data} = this.props
    return (
      <div className="card mx-auto">

        <div className="card-header">
          <h2>{data.title}</h2>
        </div>

        <div className="card-body">
          <p className="card-text">{data.description}</p>
          <button
            onClick={() => this._deleteItem()}
            className="btn btn-primary btn-lg float-right"
          >
            Удалить товар
          </button>
        </div>
      </div>
    )
  }

  _deleteItem = () => {
    const { id } = this.props.data
    console.log(id)
    DeleteItemMutation(id, () => console.log('Mutation completed'))
  }

}

export default Item;
