import React, { Component } from 'react'

class Menu extends Component {
  render() {
    return (
      <div className="form-group row">
        <label className="col-sm-2 col-form-label">Категория:</label>
        <div className="col-sm-10">
          <select className="form-control">
            <option>Телефоны</option>
            <option>Планшеты</option>
            <option>Ноутбуки</option>
          </select>
        </div>
      </div>
    )
  }
}

export default Menu
