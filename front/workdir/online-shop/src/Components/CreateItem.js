import React, { Component } from 'react';

/* Mutations */
import CreateItemMutation from '../Mutations/CreateItemMutation'

class CreateItem extends Component {


  state = {
    title: '',
    description: '',
  }

  render() {
    return (
      <div>
        <input
          value={this.state.title}
          onChange={(e) => this.setState({title: e.target.value })}
          type='text'
          placeholder='Название товара'
          className="form-control"
        />
        <br />
        <textarea
          value={this.state.description}
          onChange={(e) => this.setState({description: e.target.value })}
          type='text'
          placeholder='Описание нового товара'
          className="form-control"
          rows="3"
        />
        <br />
        <button
          onClick={() => this._createItem()}
          className="btn btn-primary btn-lg"
        >
          Добавить новый товар
        </button>
      </div>
    )
  }

  _createItem = () => {
    const { title, description } = this.state
    CreateItemMutation(title, description, () => console.log('Mutation completed'))
  }

}

export default CreateItem
