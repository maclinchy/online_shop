import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'

/* Components */
import Item from './Item'

import environment from '../Environment'

const MyQuery = graphql`
query ItemListQuery {
  items {
    edges {
      node {
        id
        title
        description
      }
    }
  }
}
`

class ItemList extends Component {
  render() {
    return (
      <QueryRenderer
        environment={environment}
        query={MyQuery}
        render={({error, props}) => {
          if (error) {
            return <div>{error.message}</div>
          } else if (props) {
            return (
              <div>
              {props.items.edges.map(({node}) =>
                  <Item
                    key={node.id}
                    data={node}
                  />
              )}
              <br />
              </div>
            )
          }
            return <div>Loading</div>
          }}
      />
    )
  }
}

export default ItemList;
