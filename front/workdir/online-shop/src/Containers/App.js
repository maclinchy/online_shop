import React, { Component } from 'react';

/* Components */
import ItemList from '../Components/ItemList'
import CreateItem from '../Components/CreateItem'
import Menu from '../Components/Menu'

/* Styles */
import 'bootstrap/dist/css/bootstrap.css'

class App extends Component {
  render() {
    return (
      <div className="container">
        <div className="jumbotron">
          <h1 className="display-3">
            Онлайн Магазин
          </h1>
          <br />
          <Menu />
        </div>

        <div>
          <ItemList />
        </div>

        <div className="jumbotron">
          <CreateItem />
        </div>

      </div>
    );
  }
}

export default App;
