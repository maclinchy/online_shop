import { createStore, combineReducers, applyMiddleware } from "redux"
import { createLogger} from "redux-logger"
import thunk from "redux-thunk"
import promise from "redux-promise-middleware"

/* Reducers */
import math from './Reducers/mathReducer'
import user from './Reducers/userReducer'

 export default createStore(
   combineReducers({
     math,
     user
   }),
   {},
   applyMiddleware(createLogger(), thunk, promise())
)
