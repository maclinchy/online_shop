import React, { Component } from "react"
import { connect } from "react-redux"

/* Components */
import { User } from '../Components/User'
import { Main } from '../Components/Main'

/* Actions */
import {setName} from '../Actions/userActions'

/* Styles */
import 'bootstrap/dist/css/bootstrap.css'

class App extends Component {
  render() {
    return (
      <div className="container">
        <Main changeUsername={() => this.props.setName("Anna")} />
        <User username={this.props.user.name} />
      </div>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    math: state.math
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setName: (name) => {
        dispatch(setName(name))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
