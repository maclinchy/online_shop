from django.db import models


class Item(models.Model):
    title = models.TextField(blank=True)
    description = models.TextField(blank=True)
