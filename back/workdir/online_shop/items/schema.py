import graphene
import django_filters
import graphql_relay
from graphene import relay
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from .models import Item


class ItemNode(DjangoObjectType):
    class Meta:
        model = Item
        interfaces = (graphene.relay.Node, )

class ItemFilter(django_filters.FilterSet):
    class Meta:
        model = Item
        fields = ['id', 'title', 'description']

class ItemCreate(graphene.relay.ClientIDMutation):
    item = graphene.Field(ItemNode)

    class Input:
        title = graphene.String()
        description = graphene.String()

    def mutate_and_get_payload(root, info, **input):
        item = Item(
            title=input.get('title'),
            description=input.get('description'),
        )
        item.save()

        return ItemCreate(item=item)

class ItemDelete(relay.ClientIDMutation):
    deleted_item_id = graphene.ID()

    class Input:
        id = graphene.ID(required=True)

    def mutate_and_get_payload(root, info, **input):
        id = input.get('id')
        pk = graphql_relay.from_global_id(id)[1]
        Item.objects.filter(pk=pk).delete()
        return ItemDelete(deleted_item_id=id)


class Query(graphene.ObjectType):
    item = relay.Node.Field(ItemNode)
    items = DjangoFilterConnectionField(ItemNode, filterset_class=ItemFilter)

class Mutation(graphene.AbstractType):
    create_item = ItemCreate.Field()
    delete_item = ItemDelete.Field()
